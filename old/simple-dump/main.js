let mssql = require("mssql");
let fs = require("fs");
let fetch = require("node-fetch");
let { promisify } = require("util");
let level = require("level");
let cbor = require("cbor");

if (
  !(
    process.env.FAKTOR_DB &&
    process.env.FAKTOR_USER &&
    process.env.FAKTOR_SERVER &&
    process.env.FAKTOR_PW
  )
) {
  console.error("missing environment");
  process.exit(-1);
}

let sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
let con;

async function main() {
  con = await mssql.connect({
    server: process.env.FAKTOR_SERVER,
    database: process.env.FAKTOR_DB,
    user: process.env.FAKTOR_USER,
    password: process.env.FAKTOR_PW,
    requestTimeout: 60000,
  });

  let result;

  /*
  /* show table structure and sample data
  */
  let tables = [
    "Udlaan",
    "Fornyelse",
    "Aflevering",
    "Filial",
    "Laaner",
    "Materiale",
    "Afdeling",
    "Opstilling",
    "Delopstilling",
    "Udgivelse",
    "Reservering",
  ];
  tables = (
    await con.query(`
  SELECT * FROM INFORMATION_SCHEMA.TABLES;
  `)
  ).recordset.map((o) => o.TABLE_NAME);
  for (let table of tables) {
    console.log(
      table,
      (await con.query("SELECT COUNT(*) FROM " + table)).recordset[0][""]
    );
    result = (await con.query(`SELECT TOP 1 * FROM ${table}`)).recordset;
    result.forEach((o) => console.log(table, JSON.stringify(o)));
  }

  console.log(
    "xxx",
    await con.query(
      `SELECT * FROM MaterialeSted WHERE MaterialeSted.ciceroId='1533833'`
    )
  );
  console.log(
    "xxx",
    await con.query(`SELECT * FROM Delopstilling WHERE BK_ciceroid='153802'`)
  );

  //*/

  /*
  // Code for extracting data for statistics app
  console.error('getting list of materials');
  //console.time('get list of materials');
  let materialer = (await con.query("SELECT DISTINCT(BK_ciceroID) FROM Materiale")).recordset.map(o => +o.BK_ciceroID);
  //console.timeEnd('get list of materials');

  console.error('fetching information about each material');
  let i = 0;
  let t0 = Date.now();
  for(let materiale of materialer) {
    materiale = (await con.query(`SELECT * FROM Materiale WHERE BK_ciceroID=${materiale}`)).recordset[0]
    let meta = fetch(`https://bibdata.dk/object/715100-katalog:${materiale.FAUST}.json`);
    let udlaan = (await con.query(`SELECT Dato, Koen, Fødselsår FROM Udlaan, Laaner WHERE Udlaan.FK_Materiale=${materiale.BK_ciceroID} AND Udlaan.FK_Laaner=Laaner.BK_LoanerInfoKey`)).recordset
    meta = await (await meta).json();

    let result = [parseInt(materiale.FAUST), materiale.Status + ' ill:' + materiale.IntraBibliotekLånMateriale, +(materiale.Anskaffelsestidspunkt || '').slice(0, 8), +(materiale.Bortskaffelsestidspunkt || '').slice(0, 8)];

    udlaan = udlaan.map(({Dato, Koen, Fødselsår}) => {
      const demo = {}
      if(Fødselsår) {
        demo.year = Fødselsår
      }
      switch(Koen) {
        case null:
        case "UNKNOWN":
          break;
        case "MALE":
          demo.gender = "m";
          break;
        case "FEMALE":
          demo.gender = "f";
          break;
        default:
          throw new Error("Invalid gender: " + Koen);
      }
      return [Dato, demo]
    });
    result.push(udlaan);

    let tags = [];
    tags.push('Type/' + meta.additionalType);
    tags = tags.concat(meta.keywords);
    tags = tags.concat(meta.creator.map(o => 'Contributor/' + o));
    tags = tags.concat(meta.contributor.map(o => 'Contributor/' + o));
    let [DK5] = tags.filter(o => o.startsWith('DK5/'));
    if(DK5) {
      DK5 = DK5.replace('DK5', "DK5*").replace(/[^0-9]*$/, '');
      while(DK5.indexOf('.') !== -1) {
        tags.push(DK5);
        DK5 = DK5.slice(0, -1)
      }
    }

    tags = Array.from(new Set(tags));
    tags.sort();
    result.push(tags);

    console.log(JSON.stringify(result));
    console.error((new Date()).toISOString().slice(0, 19), i, materialer.length, `\t\t\tETA: ${
      (new Date(t0 + (Date.now() - t0)/(i + 1) * (materialer.length + 1 - i))).toISOString().slice(0, 19)
    }`);
    if(++i> 10) break;
  }


    /*
  console.log( (await con.query(`SELECT DISTINCT(Fødselsår) FROM Laaner`)))
  /*
  console.log(
    (await con.query(`SELECT COUNT(*) FROM Udlaan`)))
  */

  /*
  console.log(
    (await con.query(`SELECT TOP 100 
    Materiale.BK_ciceroID, Materiale.FAUST, Materiale.title, Udlaan.Dato, Laaner.Koen, Laaner.Fødselsår , Anskaffelsestidspunkt, Bortskaffelsestidspunkt
    FROM Materiale, Udlaan, Laaner 
    WHERE Udlaan.FK_Materiale=Materiale.BK_ciceroID AND Udlaan.FK_Laaner=Laaner.BK_LoanerInfoKey 
    ORDER BY Materiale.BK_ciceroID, Udlaan.Dato` )))
    */

  //(await con.query("SELECT DISTINCT(Dato) FROM Udlaan ORDER BY Dato")).recordset.forEach(o => console.log(o.Dato))
  /*
  let x = (await con.query("SELECT * FROM Materiale")).recordset;
  for(let o of x) {
    console.log(JSON.stringify(o));
  }

  /*
  let materials = {};
  let x = (await con.query("SELECT BK_ciceroID, FAUST, title FROM Materiale")).recordset;
  //console.log(x[0], x.length);
  x.forEach(o => {materials[o.BK_ciceroID] = o});

  const req = new mssql.Request();
  req.stream = true;
  req.query('SELECT * FROM Udlaan');
  let prev = ''
  req.on('row', async row => {
    let material = materials[row.FK_Materiale] || {}
    let s = JSON.stringify([row.Dato, row.Tid, row.FK_Laaner, material.FAUST, material.title]);
    console.log(s);
  })
  */

  console.log("done");
  //process.exit(0);
}
main();
