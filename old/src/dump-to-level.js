let mssql = require("mssql");
let fs = require("fs");
let fetch = require("node-fetch");
let { promisify } = require("util");
let level = require("level");
let cbor = require("cbor");

if (
  !(
    process.env.FAKTOR_DB &&
    process.env.FAKTOR_USER &&
    process.env.FAKTOR_SERVER &&
    process.env.FAKTOR_PW
  )
) {
  console.error("missing environment");
  process.exit(-1);
}

let sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
let con;
let db;

async function dumpMaterialer() {
  console.log("dumpMaterialer 30s");
  console.time("dumpMaterialer");
  await getPlacements();
  await new Promise((resolve, reject) => {
    const req = new mssql.Request();
    req.stream = true;
    req.query("SELECT * FROM Materiale");
    req.on("row", async (row) => {
      let {
        BK_ciceroID,
        FAUST,
        Status,
        Anskaffelsestidspunkt,
        Bortskaffelsestidspunkt,
        IntraBibliotekLånMateriale,
        placementId,
      } = row;
      let result = {
        pid: `715100-katalog:${FAUST}`,
        cid: BK_ciceroID,
        ill: IntraBibliotekLånMateriale === "1",
        anskaffet: +Anskaffelsestidspunkt.slice(0, 8),
        status: Status,
        opstilling: placements[placementId] || {},
      };
      if (Bortskaffelsestidspunkt) {
        result.bortskaffet = +Bortskaffelsestidspunkt.slice(0, 8);
      }
      await db.put([+result.cid, "meta"], result);
    });
    req.on("done", resolve);
    req.on("error", reject);
  });
  console.timeEnd("dumpMaterialer");
}
async function dumpUdlaan() {
  console.log("dumpUdlaan 60s");
  console.time("dumpUdlaan");
  await new Promise((resolve, reject) => {
    const req = new mssql.Request();
    req.stream = true;
    req.query(
      "SELECT * FROM Udlaan, Laaner WHERE Udlaan.FK_laaner = Laaner.BK_LoanerInfoKey"
    );
    req.on("row", async (row) => {
      let { Dato, Tid, FK_Materiale, Koen, Fødselsår } = row;
      let key = [+FK_Materiale, Dato, Tid];
      let val = {};

      if (Fødselsår) {
        val.born = Fødselsår;
      }
      switch (Koen) {
        case null:
        case "UNKNOWN":
          break;
        case "MALE":
          val.gender = "m";
          break;
        case "FEMALE":
          val.gender = "f";
          break;
        default:
          throw new Error("Invalid gender: " + Koen);
      }
      db.put(key, val);
    });
    req.on("done", resolve);
    req.on("error", reject);
  });
  console.timeEnd("dumpUdlaan");
}

async function processDB() {
  let out = await promisify(fs.open)("materials.jsonl", "w");
  let o = {},
    prevCid;
  const metaCache = await level("metaCache.leveldb", {
    keyEncoding: "json",
    valueEncoding: "json",
  });
  let i = 0;
  async function process(cid) {
    let t = o[cid];
    if (t) {
      stream.pause();
      let meta;
      try {
        meta = await metaCache.get(t.pid);
      } catch (e) {}
      if (!meta) {
        try {
          let url = `https://v2.bibspire.dk/dkabm/${t.pid}`;
          //let url = `https://bibdata.dk/object/${t.pid}.json`;
          meta = await fetch(url);
          meta = await meta.json();
          await metaCache.put(t.pid, meta);
        } catch (e) {
          console.error(e);
          stream.resume();
          return;
        }
      }
      if (!meta) {
        console.error("missing meta", t.pid);
        meta = {};
      }
      let tags = [];
      for (const k in t.opstilling) tags.push(k + ": " + t.opstilling[k]);
      for (const lang of meta.languageISO6392 || []) tags.push("lang: " + lang);
      tags.push("type: " + (meta.typeBibDKType && meta.typeBibDKType[0]));
      for (const contributor of meta.creator || [])
        tags.push("contributor: " + contributor);
      for (const contributor of meta.contributor || [])
        tags.push("contributor: " + contributor);
      tags.push("date: " + (meta.date && meta.date[0]));
      let dk5 = meta.subjectDK5 && meta.subjectDK5[0];
      if (dk5) {
        tags.push("dk5: " + dk5);
        tags.push("dk5group: " + dk5.slice(0, 2));
        for (let i = 3; dk5[i] && dk5[i].match(/[0-9]/); ++i)
          tags.push("dk5group: " + dk5.slice(0, i + 1));
      }

      for (const tag of meta.subjectDK5Text || []) tags.push("tag: " + tag);
      for (const tag of meta.audience || []) tags.push("tag: " + tag);
      for (const tag of meta.subjectDBCS || []) tags.push("tag: " + tag);
      for (const tag of meta.spatialDBCS || []) tags.push("tag: " + tag);
      t.status = t.status + (t.ill ? " ILL" : "");
      tags.push("status: " + t.status);
      delete t.status;
      t.faust = t.pid.replace(/[^:]*:/, "");
      delete t.pid;
      delete t.ill;
      delete t.opstilling;
      for (const o of t.loans) {
        if (o.patron) {
          o.patron = JSON.stringify(o.patron);
        }
      }
      t.cid = parseInt(t.cid, 10);
      tags = Array.from(new Set(tags));
      tags.sort();
      t.tags = tags;

      console.log(++i, t);
      fs.writeSync(out, JSON.stringify(t) + "\n");
      stream.resume();
    }
    delete o[cid];
  }
  const stream = db
    .createReadStream()
    .on("data", function (data) {
      let [cid, date] = data.key;
      o[cid] = o[cid] || { loans: [] };
      if (date === "meta") {
        Object.assign(o[cid], data.value);
      } else {
        o[cid].loans.push({ date, patron: data.value });
      }
      if (cid !== prevCid) {
        process(prevCid);
        prevCid = cid;
      }
      //console.log(data.key, '=', data.value)
    })
    .on("end", async function () {
      process(prevCid);
      fs.close(out);
      await sleep(2000);
      process.exit(0);
    });
}

let placements;
async function getPlacements() {
  const afdelinger = {};
  for (const table of ["Afdeling", "Opstilling", "Delopstilling"]) {
    let result = (await con.query(`SELECT BK_ciceroid, Afdeling FROM ${table}`))
      .recordset;
    for (const { BK_ciceroid, Afdeling } of result) {
      afdelinger[BK_ciceroid] = Afdeling;
    }
  }

    let result = (await con.query(`SELECT BK_ciceroId, Filial FROM Filial`))
      .recordset;
    for (const { BK_ciceroId, Filial} of result) {
      afdelinger[BK_ciceroId] = Filial;
    }

  placements = {};
  (await con.query(`SELECT * FROM MaterialeSted`)).recordset.forEach((o) => {
    let result = {};
    if (o.BranchId !== "0")
      result.filial = afdelinger[o.BranchId] || o.BranchId;
    if (o.locationId !== "0")
    if (o.departmentId !== "0")
      result.afdeling = afdelinger[o.departmentId] || o.departmentId;
    if (o.locationId !== "0")
      result.opstilling = afdelinger[o.locationId] || o.locationId;
    if (o.sublocationId !== "0")
      result.delopstilling = afdelinger[o.sublocationId] || o.sublocationId;
    placements[o.ciceroId] = result;
  });
}

async function main() {
  con = await mssql.connect({
    server: process.env.FAKTOR_SERVER,
    database: process.env.FAKTOR_DB,
    user: process.env.FAKTOR_USER,
    password: process.env.FAKTOR_PW,
    requestTimeout: 60000,
  });
  db = await level("db.leveldb", {
    keyEncoding: "json",
    valueEncoding: "json",
  });

  await dumpMaterialer();
  await dumpUdlaan();
  await processDB();

  //console.log( (await con.query(`SELECT TOP 1 *  FROM Laaner`)))
  //  return;

  let result;

  /*
  /* show table structure and sample data
  */
  let tables = [
    "Udlaan",
    "Fornyelse",
    "Aflevering",
    "Filial",
    "Laaner",
    "Materiale",
    "Afdeling",
    "Opstilling",
    "Delopstilling",
    "Udgivelse",
    "Reservering",
  ];
  tables = (
    await con.query(`
  SELECT * FROM INFORMATION_SCHEMA.TABLES;
  `)
  ).recordset.map((o) => o.TABLE_NAME);
  for (let table of tables) {
    console.log(
      table,
      (await con.query("SELECT COUNT(*) FROM " + table)).recordset[0][""]
    );
    result = (await con.query(`SELECT TOP 1 * FROM ${table}`)).recordset;
    result.forEach((o) => console.log(table, JSON.stringify(o)));
  }

  //console.log( (await con.query(`SELECT TOP 10 placementId  FROM Materiale`)))

  //*/

  /*
  // Code for extracting data for statistics app
  console.error('getting list of materials');
  //console.time('get list of materials');
  let materialer = (await con.query("SELECT DISTINCT(BK_ciceroID) FROM Materiale")).recordset.map(o => +o.BK_ciceroID);
  //console.timeEnd('get list of materials');

  console.error('fetching information about each material');
  let i = 0;
  let t0 = Date.now();
  for(let materiale of materialer) {
    materiale = (await con.query(`SELECT * FROM Materiale WHERE BK_ciceroID=${materiale}`)).recordset[0]
    let meta = fetch(`https://bibdata.dk/object/715100-katalog:${materiale.FAUST}.json`);
    let udlaan = (await con.query(`SELECT Dato, Koen, Fødselsår FROM Udlaan, Laaner WHERE Udlaan.FK_Materiale=${materiale.BK_ciceroID} AND Udlaan.FK_Laaner=Laaner.BK_LoanerInfoKey`)).recordset
    meta = await (await meta).json();

    let result = [parseInt(materiale.FAUST), materiale.Status + ' ill:' + materiale.IntraBibliotekLånMateriale, +(materiale.Anskaffelsestidspunkt || '').slice(0, 8), +(materiale.Bortskaffelsestidspunkt || '').slice(0, 8)];

    udlaan = udlaan.map(({Dato, Koen, Fødselsår}) => {
      const demo = {}
      if(Fødselsår) {
        demo.year = Fødselsår
      }
      switch(Koen) {
        case null:
        case "UNKNOWN":
          break;
        case "MALE":
          demo.gender = "m";
          break;
        case "FEMALE":
          demo.gender = "f";
          break;
        default:
          throw new Error("Invalid gender: " + Koen);
      }
      return [Dato, demo]
    });
    result.push(udlaan);

    let tags = [];
    tags.push('Type/' + meta.additionalType);
    tags = tags.concat(meta.keywords);
    tags = tags.concat(meta.creator.map(o => 'Contributor/' + o));
    tags = tags.concat(meta.contributor.map(o => 'Contributor/' + o));
    let [DK5] = tags.filter(o => o.startsWith('DK5/'));
    if(DK5) {
      DK5 = DK5.replace('DK5', "DK5*").replace(/[^0-9]*$/, '');
      while(DK5.indexOf('.') !== -1) {
        tags.push(DK5);
        DK5 = DK5.slice(0, -1)
      }
    }

    tags = Array.from(new Set(tags));
    tags.sort();
    result.push(tags);

    console.log(JSON.stringify(result));
    console.error((new Date()).toISOString().slice(0, 19), i, materialer.length, `\t\t\tETA: ${
      (new Date(t0 + (Date.now() - t0)/(i + 1) * (materialer.length + 1 - i))).toISOString().slice(0, 19)
    }`);
    if(++i> 10) break;
  }


    /*
  console.log( (await con.query(`SELECT DISTINCT(Fødselsår) FROM Laaner`)))
  /*
  console.log(
    (await con.query(`SELECT COUNT(*) FROM Udlaan`)))
  */

  /*
  console.log(
    (await con.query(`SELECT TOP 100 
    Materiale.BK_ciceroID, Materiale.FAUST, Materiale.title, Udlaan.Dato, Laaner.Koen, Laaner.Fødselsår , Anskaffelsestidspunkt, Bortskaffelsestidspunkt
    FROM Materiale, Udlaan, Laaner 
    WHERE Udlaan.FK_Materiale=Materiale.BK_ciceroID AND Udlaan.FK_Laaner=Laaner.BK_LoanerInfoKey 
    ORDER BY Materiale.BK_ciceroID, Udlaan.Dato` )))
    */

  //(await con.query("SELECT DISTINCT(Dato) FROM Udlaan ORDER BY Dato")).recordset.forEach(o => console.log(o.Dato))
  /*
  let x = (await con.query("SELECT * FROM Materiale")).recordset;
  for(let o of x) {
    console.log(JSON.stringify(o));
  }

  /*
  let materials = {};
  let x = (await con.query("SELECT BK_ciceroID, FAUST, title FROM Materiale")).recordset;
  //console.log(x[0], x.length);
  x.forEach(o => {materials[o.BK_ciceroID] = o});

  const req = new mssql.Request();
  req.stream = true;
  req.query('SELECT * FROM Udlaan');
  let prev = ''
  req.on('row', async row => {
    let material = materials[row.FK_Materiale] || {}
    let s = JSON.stringify([row.Dato, row.Tid, row.FK_Laaner, material.FAUST, material.title]);
    console.log(s);
  })
  */

  console.log("done");
  //process.exit(0);
}
main();
