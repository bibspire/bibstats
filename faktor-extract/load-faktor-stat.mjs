// # Transfer FAKTOR-data to mysql
//
// # Setup
import mssql from "mssql";
import { promisify } from "util";
import { sql } from "./util.mjs";

if (
  !(
    process.env.FAKTOR_DB &&
    process.env.FAKTOR_USER &&
    process.env.FAKTOR_SERVER &&
    process.env.FAKTOR_PW
  )
) {
  console.error("missing environment");
  process.exit(-1);
}
let con;

// # Util

let sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
function td2date(d, t) {
  t = String(10000 + t).slice(1);
  t = t.slice(0, 2) + ":" + t.slice(2);
  d = String(d);
  d = d.slice(0, 4) + "-" + d.slice(4, 6) + "-" + d.slice(6);
  return d + "T" + t + "Z";
}

function sToDate(s) {
  if (!s) return null;
  return new Date(
    s.slice(0, 4) +
      "-" +
      s.slice(4, 6) +
      "-" +
      s.slice(6, 8) +
      "T" +
      s.slice(8, 10) +
      ":" +
      s.slice(10, 12) +
      ":" +
      s.slice(12) +
      "Z"
  );
}

// # Transfer code
async function transfer_faktor_data() {
  let result, row;
  let stedCache = {};
  // ## Initialisation
  con = await mssql.connect({
    server: process.env.FAKTOR_SERVER,
    database: process.env.FAKTOR_DB,
    user: process.env.FAKTOR_USER,
    password: process.env.FAKTOR_PW,
    requestTimeout: 60000,
    options: {
      enableArithAbort: true,
    },
  });

  for (const table of "faktor_laan faktor_fornyelse faktor_aflevering".split(
    " "
  ))
    result = await sql.query(`
    CREATE TABLE IF NOT EXISTS ${table} (
      date TIMESTAMP NOT NULL,
      material INT NOT NULL,
      patron INT NOT NULL,
      location INT NOT NULL,
      PRIMARY KEY (date, material, patron),
      KEY (material, date),
      KEY (patron, date)
   )`);
  result = await sql.query(`
    CREATE TABLE IF NOT EXISTS faktor_lokation (
      id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
      agency INT NOT NULL,
      description TEXT UNIQUE KEY
   )`);

  result = await sql.query(`
   CREATE TABLE IF NOT EXISTS faktor_materiale (
      id INTEGER PRIMARY KEY,
      faust VARCHAR(255),
      status ENUM('LOST', 'SENT_FROM_SUPPLIER', 'ORDERED', 
          'DISCARDED', 'LENDOUT', 'READY_FOR_PICKUP',
          'NOT_DELIVERED', 'IN_TRANSIT', 'AVAILABLE',
          'CLOSED_ILL'),
      anskaffet DATETIME, 
      bortskaffet DATETIME, 
      tidskrift TEXT,
      interlibrary BOOLEAN,
      tema TINYTEXT,
      location INT,
      KEY (faust)
     );`);

  result = await sql.query(`
    CREATE TABLE IF NOT EXISTS faktor_patron (
      id INT NOT NULL PRIMARY KEY,
      gender ENUM('MALE', 'FEMALE', 'UNKNOWN'),
      birthyear SMALLINT,
      type ENUM('PERSON', 'COMPANY', 'GROUP', 'LIBRARY'),
      isil VARCHAR(255),
      postnr VARCHAR(255)
   )`);

  // ## Util
  async function stedId({
    agency,
    filial,
    afdeling,
    opstilling,
    delopstilling,
  }) {
    let id,
      sted = {};
    if (filialer[filial]) sted.filial = filialer[filial];
    if (locs[afdeling]) sted.afdeling = locs[afdeling];
    if (locs[opstilling]) sted.opstilling = locs[opstilling];
    if (locs[delopstilling]) sted.delopstilling = locs[delopstilling];
    sted = JSON.stringify(sted);
    id = stedCache[sted];
    if (id) return id;

    id = (
      (
        await sql.query("SELECT id FROM faktor_lokation WHERE description=?", [
          sted,
        ])
      )[0][0] || {}
    ).id;
    if (id) return id;

    await sql.query(
      "INSERT INTO faktor_lokation (agency, description) VALUES(?)",
      [[agency, sted]]
    );
    id = (
      (
        await sql.query("SELECT id FROM faktor_lokation WHERE description=?", [
          sted,
        ])
      )[0][0] || {}
    ).id;
    return id;
  }
  async function handlePatronActionRows(rows) {
    let result = [];
    for (let row of rows.recordset) {
      let sted = await stedId({
        agency: 715100,
        afdeling: row.FK_Afdeling,
        opstilling: row.FK_Opstilling,
        delopstilling: row.FK_Delopstilling,
      });
      row = [
        new Date(td2date(row.Dato, row.Tid)),
        row.FK_Materiale,
        row.FK_Laaner,
        sted,
      ];
      result.push(row);
    }
    return result;
  }
  async function maxDateNumber(table) {
    return +(
      (await sql.query(`SELECT MAX(date) FROM ${table}`))[0][0]["MAX(date)"] ||
      new Date("1970-01-01")
    )
      .toISOString()
      .replace(/-/g, "")
      .replace(/T.*/, "");
  }

  // ## Code for creation location/filial/... data
  let locs = {};
  function addLoc(row) {
    let afdeling = locs[row.BK_ciceroid];
    if (afdeling && afdeling !== row.Afdeling) {
      //console.error("WARNING: duplicate loc:", row.Afdeling, " ––– ", afdeling);
    }
    locs[row.BK_ciceroid] = row.Afdeling;
  }
  for (const row of (await con.query(`SELECT * FROM Afdeling`)).recordset)
    addLoc(row);

  for (const row of (await con.query(`SELECT * FROM Opstilling`)).recordset)
    addLoc(row);

  for (const row of (await con.query(`SELECT * FROM Delopstilling`)).recordset)
    addLoc(row);

  let filialer = {};
  for (const filial of (await con.query(`SELECT * FROM Filial`)).recordset)
    filialer[filial.BK_ciceroId] = filial.Filial;

  let temaer = {};
  for (const tema of (await con.query(`SELECT * FROM Tema`)).recordset)
    temaer[tema.ciceroid] = tema.TemaNavn;

  const materialeSted = {};
  for (const row of (await con.query(`SELECT * FROM MaterialeSted`))
    .recordset) {
    materialeSted[row.ciceroId] = row;
  }

  // ## Test code dumping schema
  let tables = (
    await con.query(`
  SELECT * FROM INFORMATION_SCHEMA.TABLES;
  `)
  ).recordset.map((o) => o.TABLE_NAME);

  console.log(tables);
  for (let table of ["Laaner"] /*tables*/) {
    console.log(
      table,
      (await con.query("SELECT COUNT(*) FROM " + table)).recordset[0][""]
    );
    result = (await con.query(`SELECT TOP 1 * FROM ${table}`)).recordset;
    result.forEach((o) => console.log(table, JSON.stringify(o)));
  }
  //result = await(con.query`SELECT DISTINCT(Postnummer) from Laaner`);
  //console.log(result.recordset);

  //console.log(result);

  // ## transfer laaner
  let patrons;
  do {
    let maxId =
      ((await sql.query("SELECT MAX(id) FROM faktor_patron"))[0][0] || {})[
        "MAX(id)"
      ] || 0;
    patrons = [];
    for (row of (
      await con.query`SELECT TOP 5000 * FROM Laaner WHERE BK_LoanerInfoKey>${maxId} ORDER BY BK_LoanerInfoKey`
      //await con.query`SELECT TOP 5000 * FROM Laaner WHERE BK_ciceroID>${maxId} ORDER BY BK_ciceroID`
    ).recordset) {
      patrons.push([row.BK_LoanerInfoKey, row.Koen, row["Fødselsår"], row.Laanertype, row.LaanerBibliotekISIL, row.Postnummer])
      console.log(row);
    }
    if(patrons.length!== 0) {
      let result = await sql.query(`REPLACE INTO faktor_patron 
        (id,  gender, birthyear, type, isil, postnr) VALUES ?`,
        [patrons]);
      console.log(`added ${result.length} faktor_laan`);
    }

    console.log(patrons);
    /*
    materials = [];
      let o = materialeSted[row.placementId];
      //console.log(row, o)
      let tidskrift = {};
      if (o.periodicalNumber) tidskrift.number = o.periodicalNumber;
      if (o.periodicalVolume) tidskrift.volume = o.periodicalVolume;
      if (o.periodicalYear) tidskrift.year = o.periodicalYear;

      let m = {
        id: row.BK_ciceroID,
        faust: row.FAUST,
        status: row.Status,
        anskaffet: sToDate(row.Anskaffelsestidspunkt),
        bortskaffet: sToDate(row.Bortskaffelsestidspunkt),
        tidskrift: JSON.stringify(tidskrift),
        interlibrary: row.IntraBibliotekLånMateriale === "1",
        tema: temaer[row.TemaID],
        placering: await stedId({
          agency: 715100,
          filial: o.FilialID,
          afdeling: o.AfdelingID,
          opstilling: o.OpstillingId,
          delopstilling: o.DelopstillingId,
        }),
      };
      materials.push([
        m.id,
        m.faust,
        m.status,
        m.anskaffet,
        m.bortskaffet,
        m.tidskrift,
        m.interlibrary,
        m.tema,
        m.placering,
      ]);
    }

    if (materials.length)
      await sql.query(
        `REPLACE INTO faktor_materiale (id, faust, status, anskaffet, bortskaffet, tidskrift, interlibrary, tema, location) VALUES ?`,
        [materials]
      );
  */
  } while (patrons.length === 5000);

  // ## transfer materials
  let materials;
  do {
    let maxId =
      ((await sql.query("SELECT MAX(id) FROM faktor_materiale"))[0][0] || {})[
        "MAX(id)"
      ] || 0;
    materials = [];
    for (row of (
      await con.query`SELECT TOP 5000 * FROM Materiale WHERE BK_ciceroID>${maxId} ORDER BY BK_ciceroID`
    ).recordset) {
      let o = materialeSted[row.placementId];
      //console.log(row, o)
      let tidskrift = {};
      if (o.periodicalNumber) tidskrift.number = o.periodicalNumber;
      if (o.periodicalVolume) tidskrift.volume = o.periodicalVolume;
      if (o.periodicalYear) tidskrift.year = o.periodicalYear;

      let m = {
        id: row.BK_ciceroID,
        faust: row.FAUST,
        status: row.Status,
        anskaffet: sToDate(row.Anskaffelsestidspunkt),
        bortskaffet: sToDate(row.Bortskaffelsestidspunkt),
        tidskrift: JSON.stringify(tidskrift),
        interlibrary: row.IntraBibliotekLånMateriale === "1",
        tema: temaer[row.TemaID],
        placering: await stedId({
          agency: 715100,
          filial: o.FilialID,
          afdeling: o.AfdelingID,
          opstilling: o.OpstillingId,
          delopstilling: o.DelopstillingId,
        }),
      };
      materials.push([
        m.id,
        m.faust,
        m.status,
        m.anskaffet,
        m.bortskaffet,
        m.tidskrift,
        m.interlibrary,
        m.tema,
        m.placering,
      ]);
    }

    if (materials.length)
      await sql.query(
        `REPLACE INTO faktor_materiale (id, faust, status, anskaffet, bortskaffet, tidskrift, interlibrary, tema, location) VALUES ?`,
        [materials]
      );
  } while (materials.length === 5000);
  result = await con.query("SELECT DISTINCT status FROM Materiale");
  //console.log(result.recordset);

  // ## transfer udlån, aflevering, fornyelse
  do {
    result = await con.query`SELECT TOP 20000 * FROM Udlaan WHERE Dato >= ${await maxDateNumber(
      "faktor_laan"
    )} ORDER BY Dato`;
    result = await handlePatronActionRows(result);
    if (result.length) {
      await sql.query(
        "REPLACE INTO faktor_laan (date, material, patron, location) VALUES ?",
        [result]
      );
      console.log(`added ${result.length} faktor_laan`);
    }
  } while (result.length === 20000);

  do {
    result = await con.query`SELECT TOP 20000 * FROM Fornyelse WHERE Dato >= ${await maxDateNumber(
      "faktor_fornyelse"
    )} ORDER BY Dato`;
    result = await handlePatronActionRows(result);
    if (result.length) {
      await sql.query(
        "REPLACE INTO faktor_fornyelse(date, material, patron, location) VALUES ?",
        [result]
      );
      console.log(`added ${result.length} faktor_fornyelse`);
    }
  } while (result.length === 20000);

  do {
    result = await con.query`SELECT TOP 20000 * FROM Aflevering WHERE Dato >= ${await maxDateNumber(
      "faktor_aflevering"
    )} ORDER BY Dato`;
    result = await handlePatronActionRows(result);
    if (result.length) {
      await sql.query(
        "REPLACE INTO faktor_aflevering (date, material, patron, location) VALUES ?",
        [result]
      );
      console.log(`added ${result.length} faktor_aflevering`);
    }
  } while (result.length === 20000);

  // ## Done
  // technobunker
  console.log("done");
}
// # Run
transfer_faktor_data();
