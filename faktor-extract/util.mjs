import mysql from "mysql2/promise";
import fs from "fs";
import readline from "readline";
import zlib from "zlib";
import btoa from "btoa";
import crypto from "crypto";
import { promisify } from "util";

export function filterDuplicates(arr) {
    const seen = {};
    return arr.filter((o) => {
          let isSeen = seen[o];
          seen[o] = true;
          return !isSeen;
        });
}

export const sql = mysql.createPool({
  host: "localhost",
  user: process.env.VEDUZ_DB_USER,
  password: process.env.VEDUZ_DB_PASSWORD,
  database: process.env.VEDUZ_DB,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
});

export async function forLineInFile(filename, f) {
  console.time("process lines in " + filename);
  let fileStream = fs.createReadStream(filename);
  if (filename.endsWith(".gz")) {
    const gunzip = zlib.createGunzip();
    fileStream = fileStream.pipe(gunzip);
  }
  try {
    const rl = readline.createInterface({
      input: fileStream,
      crlfDelay: Infinity,
    });
    for await (const line of rl) {
      await f(line);
    }
  } catch (e) {
    console.log(e);
  }
  fileStream.close();
  console.timeEnd("process lines in " + filename);
}
