# BibStats.dk

Simpelt proof-of-concept statistikværktøj.


# Noter

## UI design

Matrix med filtre og visninger

Filtre:
- Filial, afdeling, opstilling, delopstilling
- DK5
- (nøgleord)

Visninger + tidskonfiguration:
- Beholdningsalder - liggende summeret barchart med årstal...
- Antal udlån
- Antal materialer
- Cirkulationstal (lån / antal materialer i perioden)
- Tilvækst
- Afgang
- køn / alder

----

Back-of-envelope datamodel og udregning om data for ballerup bibliotek kan være i hukommelse i browser/web-applikation:

```
- Materialer ca. 300K – ca.x 50B = ca. 15MB.
    - Faustnummer 4B
    - Anskaffelsesdato 2B
    - Bortskaffelsesdato 2B
    - ¿10?x Metadata, opstilling etc. ca 2B per indgang  
    - Liste af udlån: ca. 1B length
- ca. 3M udlån x 4B – ca. 12MB
    - 2B dato
    - 2B alder + demografi
- Metadata-strenge: 100K? x 30B? = 3M


Total: ca. 30MB 
```

I praksis 18MB, – men også med et par yderligere optimeringer.
